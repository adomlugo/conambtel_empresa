# conambtel_empresa

## Descripción
Este proyecto permite a la empresa la realización de consultas SQL al repositorio central de MariaDB, gestionar teletrabajadores y parametrizar
los rangos de condiciones ambientales ideales

## Requisitos
Para que este proyecto pueda funcionar se requiere que el computador a implementar se haya instalado lo siguiente:
Python en su version 3.11.6
git
repositorio central en MariaDB creado y configurado
parametros de conexión al repositorio central de MariaDB
accesos con un usuario y una contraseña al repositorio central de MariaDB que tenga permisos de lectura/escritura sobre la base de datos conambtel

## Instrucciones de Implementación

abrir la ventana de comandos de windows y en ella ubicarse con el comando cd en un directorio donde se pretende instalar el programa y seguir las sisguientes 
instrucciones:

clonar el repositorio
```
git clone https://gitlab.com/adomlugo/conambtel_empresa.git
cd conambtel_empresa
```

Instalar el ambiente virtual de python
```
python -m venv venv
```
Activar el ambiente virtual
```
cd venv\Scripts
activate
cd ..\..
```
Instalar los requerimientos del software
```
pip install -r requirements.txt
```
editar el archivo de configuración "configuración.yml" llenando cada parámetro de configuración de acuerdo a los requerimientos:
```
empresa:
  id: <colocar aqui el id de la empresa. ejemplo id: 1>
base_de_datos:
  host: <colocar la url de conexión a la base de datos de MariaDB ejemplo host: localhost o host: xxxxxx.us-east-1.rds.amazonaws.com>
  port: <puerto de la instancia de MariaDB ejemplo port: 3306>
  database: conambtel
  user: <usuario con acceso lectura escritura a la base de datos de MariaDB ejemplo user: conan>
  pass: <clave del usuario anterior ejemplo pass: miclave>
```

ejecutar el programa
```
python main.py
```
