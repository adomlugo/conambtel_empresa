import sys
import os
import yaml
import pymysql
import logging as log
from nicegui import app, ui
import csv
import uuid
import hashlib

from fastapi import Request
from fastapi.responses import RedirectResponse
from starlette.middleware.base import BaseHTTPMiddleware

# declara el folder donde existen recursos estáticos (imagenes)
app.add_static_files("/static", "static")

# constantes del programa
NOMBRE_FICHERO_CONFIGURACIÓN = "configuración.yml"

# rutas que no necesitan autenticación
unrestricted_page_routes = {'/login','/static/telework.jpg'}

# clase Middleware que permite restringir el uso de rutas no autenticadas
class AuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if not app.storage.user.get('authenticated', False):
            if (request.url.path not in unrestricted_page_routes) and ('/_nicegui' not in request.url.path) and ('/login/favicon.ico' not in request.url.path):
                return RedirectResponse('/login')
        return await call_next(request)

# activación de la clase que permite restringir rutas no autenticadas con usuario y password
app.add_middleware(AuthMiddleware)

# **************************************************************
# *** Funciones locales del programa no expuestas al frontend **
# Leer archivo de configuracion local
def leer_configuración_local(ruta_fichero_configuración):
    contenido = ""
    with open(ruta_fichero_configuración,"r") as fichero_configuracion:
        contenido = yaml.safe_load(fichero_configuracion)
    return contenido

# procedimiento que permite obtener el acceso a la base de datos
# centralizada de MariaDB através de la variable global conexion_remota
def obtener_conexión_remota():
    global configuración
    conexion_remota = None
    try:
        conexion_remota = pymysql.connect(
            host=configuración["base_de_datos"]["host"],
            port=configuración["base_de_datos"]["port"],
            user=configuración["base_de_datos"]["user"],
            passwd=configuración["base_de_datos"]["pass"],
            db=configuración["base_de_datos"]["database"],
            cursorclass=pymysql.cursors.DictCursor
        )
        conexion_remota.autocommit(True)
    except:
        log.error(f'conectando a base de datos remota, revise configuración local')
        sys.exit(1)
    return conexion_remota

# Leer parámetros del sistema almacenados en el reporsitorio central para
# la empresa configurada en el archivo de configuración
def leer_configuración_remota():
    global configuración
    global conexion_remota
    global conf_nit
    global conf_nombre
    global conf_usuario
    global conf_contraseña
    global conf_host_correo_saliente
    global conf_usuario_correo_saliente
    global conf_contraseña_correo_saliente
    global conf_de_correo_saliente
    global conf_para_correo_saliente
    global conf_puerto_correo_saliente
    global conf_luz_mínima
    global conf_luz_máxima
    global conf_ruido_mínimo
    global conf_ruido_máximo
    global conf_temperatura_mínima
    global conf_temperatura_máxima
    global conf_distancia_ideal
    global conf_tiempo_pausa_activa
    global conf_espera_entre_notificaciones
    global conf_espera_entre_envíos_a_repositorio
    
    with conexion_remota.cursor() as cursor:
        sql = """ 
            SELECT
                nit, 
                nombre,
                usuario,
                contraseña, 
                host_correo_saliente, 
                usuario_correo_saliente, 
                contraseña_correo_saliente, 
                de_correo_saliente, 
                para_correo_saliente, 
                puerto_correo_saliente,
                luz_mínima,
                luz_máxima,
                ruido_mínimo,
                ruido_máximo,
                temperatura_mínima,
                temperatura_máxima,
                distancia_ideal,
                tiempo_pausa_activa,
                espera_entre_notificaciones,
                espera_entre_envíos_a_repositorio
            FROM 
                EMPRESA
            WHERE
                empresa_id = %s
        """
        cursor.execute(sql,(configuración["empresa"]["id"],))
        resultado = cursor.fetchone()
        conf_nit=resultado['nit']
        conf_nombre=resultado['nombre']
        conf_usuario=resultado['usuario']
        conf_contraseña=resultado['contraseña']
        conf_host_correo_saliente=resultado['host_correo_saliente']
        conf_usuario_correo_saliente=resultado['usuario_correo_saliente']
        conf_contraseña_correo_saliente=resultado['contraseña_correo_saliente']
        conf_de_correo_saliente=resultado['de_correo_saliente']
        conf_para_correo_saliente=resultado['para_correo_saliente']
        conf_puerto_correo_saliente=str(resultado['puerto_correo_saliente'])
        conf_luz_mínima=resultado['luz_mínima']
        conf_luz_máxima=resultado['luz_máxima']
        conf_ruido_mínimo=resultado['ruido_mínimo']
        conf_ruido_máximo=resultado['ruido_máximo']
        conf_temperatura_mínima=resultado['temperatura_mínima']
        conf_temperatura_máxima=resultado['temperatura_máxima']
        conf_distancia_ideal=resultado['distancia_ideal']
        conf_tiempo_pausa_activa=resultado['tiempo_pausa_activa']
        conf_espera_entre_notificaciones=resultado['espera_entre_notificaciones']
        conf_espera_entre_envíos_a_repositorio=resultado['espera_entre_envíos_a_repositorio']

# **************************************************************
# *** Variables globales de la aplicación ***
directorio_actual = None
configuración = None
conexion_remota = None
puerto = None
sensor = {}
menu_seleccionado ="color:#fff;border-left:solid 4px #3c8dbc"
menu_no_seleccionado ="color:#fff;border-left:solid 4px #222d32"
conf_nit=""
conf_nombre=""
conf_usuario=""
conf_contraseña=""
conf_host_correo_saliente=""
conf_usuario_correo_saliente=""
conf_contraseña_correo_saliente=""
conf_de_correo_saliente=""
conf_para_correo_saliente=""
conf_puerto_correo_saliente=""
conf_luz_mínima=0
conf_luz_máxima=0
conf_ruido_mínimo=0
conf_ruido_máximo=0
conf_temperatura_mínima=0
conf_temperatura_máxima=0
conf_distancia_ideal=0
conf_tiempo_pausa_activa=0
conf_espera_entre_notificaciones=0
conf_espera_entre_envíos_a_repositorio=0
glob_sql=""
glob_grilla={
    'defaultColDef': {'flex': 1},
    'columnDefs': [
        {'headerName': 'Campo 1', 'field': 'campo1'},
    ],
    'rowData': [
        {'campo1': None},
    ],
    'rowSelection': 'multiple',
}
glob_borrar=False
glob_sel_ciudad=None
trab_id=0
trab_empresa_id=0
trab_cédula=""
trab_nombre=""
trab_género=""
trab_fecha_nacimiento=""
trab_fecha_ingreso=""
trab_departamento_id=0
trab_ciudad_id=0
trab_correo=""

# componentes que permiten refrescar sus datos
@ui.refreshable
def grilla_teletrabajadores(teletrabajadores):
    ui.table(
        title="Listado de teletrabajadores",
        columns= [
            {'name': 'teletrabajador_id', 'label': 'Id', 'field': 'teletrabajador_id', 'align': 'right'},
            {'name': 'nombre', 'label': 'Nombre', 'field': 'nombre', 'sortable': True,'align': 'left'},
        ], 
        rows=teletrabajadores,
        row_key='teletrabajador_id', 
        selection="single",
        pagination={'rowsPerPage': 7, 'sortBy': 'teletrabajador_id', 'page': 1},
        on_select=tabla_select 
    ).classes("w-96 py-4")

# punto de entrada para la página de autenticación del sistema
@ui.page('/login',favicon='pausa_activa.ico', title="ConAmbTel")
def login():
    ui.add_head_html('''
        <style type="text/tailwindcss">
            body {
                background-image: url('/static/telework.jpg');
                background-repeat: no-repeat;
                background-size:cover;
            }
        </style>
    ''')
    def try_login():
        global conexion_remota
        global configuración
        try:
            with conexion_remota.cursor() as cursor:
                sql = """ 
                    SELECT count(*) as n
                    FROM
                        EMPRESA
                    WHERE
                        empresa_id = %s AND usuario = %s AND contraseña = %s
                """
                
                cursor.execute(sql,(configuración["empresa"]["id"],usuario.value,hashlib.md5(clave.value.encode()).hexdigest()))
                resultado = cursor.fetchone()
                if(resultado["n"] == 1):
                    app.storage.user.update({'username': usuario.value, 'authenticated': True})
                    ui.open('inicio')
                else:
                    ui.notify('Error de autenticación', close_button='OK',type='negative')
        except:
                ui.notify('Error de conexión', close_button='OK',type='negative')
    with ui.card().classes('absolute-center'):
        ui.label("SISTEMA DE CONSULTA DE CONDICIONES AMBIENTALES").classes('font-bold')
        ui.separator()
        with ui.row().classes('pt-4'):
            ui.label('Usuario:').classes('w-32')
            usuario = ui.input().on('keydown.enter', try_login).props('outlined dense').classes('w-64')
        with ui.row():
            ui.label('Contraseña:').classes('w-32')
            clave = ui.input( password=True, password_toggle_button=True).on('keydown.enter', try_login).props('outlined dense').classes('w-64')
        ui.button('Log in', on_click=try_login)
    return None

# punto de entrada para todas las páginas de la aplicación excepto la del login
@ui.page('/',favicon='pausa_activa.ico', title="ConAmbTel")
@ui.page('/{pagina}',favicon='pausa_activa.ico', title="ConAmbTel")
def page_layout(pagina="inicio"):
    global glob_sel_ciudad
    ui.add_head_html('''
        <style>
            :root {
                --nicegui-default-padding: 16px;
                --nicegui-default-gap: 8px;
            }
        </style>
    ''')
    with ui.header(elevated=True).style('background-color: #3c8dbc' ).classes('items-center'):
        ui.button(on_click=lambda: left_drawer.toggle(), icon='menu').props('flat color=white')
        ui.label('SISTEMA DE CONSULTAS')
    with ui.left_drawer(top_corner=False, bottom_corner=False).style('background-color: #222d32; padding:0px 0px') as left_drawer:
        with ui.list().props('bordered separator').style("width: 100%"):
            ui.item_label('MENU PRINCIPAL').props('header').classes('text-bold').style("color:#fff")
            ui.separator()
            if pagina=="inicio":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('inicio')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('grid_on')
                with ui.item_section():
                    ui.item_label('Consultar al sistema')
            
            if pagina=="teletrabajadores":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('teletrabajadores')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('supervised_user_circle').style("color:#fff")
                with ui.item_section().style("color:#fff"):
                    ui.item_label('Gestionar teletrabajadores')
            if pagina=="ver_configuración":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('ver_configuración')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('add_to_queue').style("color:#fff")
                with ui.item_section().style("color:#fff"):
                    ui.item_label('Configurar parámetros')
            if pagina=="salir":
                menu = menu_seleccionado
            else:
                menu = menu_no_seleccionado
            with ui.item(on_click=lambda: ui.open('salir')).style(menu):
                with ui.item_section().props('avatar'):
                    ui.icon('grid_on')
                with ui.item_section():
                    ui.item_label('Salir')
    with ui.footer().style('background-color: #333'):
        ui.label('ADL 2024')
    if pagina=="inicio":
        leer_configuración_remota()
        with ui.card():
            ui.label('TABLAS DISPONIBLES').classes('font-bold')
        with ui.row().classes('w-full'):
            with ui.card().classes('w-64 h-72'):
                with ui.scroll_area():
                    ui.tree([
                        {'id': 'EMPRESA', 'children': [{'id': 'empresa_id'}, {'id': 'nit'}, {'id': 'nombre'},{'id': 'luz_mínima'},{'id': 'luz_máxima'},{'id': 'ruido_mínimo'},{'id': 'ruido_máximo'},{'id': 'temperatura_mínima'},{'id': 'temperatura_máxima'},{'id': 'distancia_ideal'},{'id': 'tiempo_pausa_activa'}]},
                        {'id': 'TELETRABAJADOR', 'children': [{'id': 'teletrabajador_id'},{'id': 'empresa_id'}, {'id': 'cédula'}, {'id': 'nombre'}, {'id': 'género'}, {'id': 'fecha_nacimiento'}, {'id': 'fecha_ingreso'}, {'id': 'ciudad_id'}, {'id': 'correo'}]},
                        {'id': 'DEPARTAMENTO', 'children': [{'id': 'departamento_id'}, {'id': 'nombre'}]},
                        {'id': 'CIUDAD', 'children': [{'id': 'ciudad_id'}, {'id': 'departamento_id'},{'id': 'nombre'}]},
                        {'id': 'MEDICIÓN', 'children': [{'id': 'medición_id'}, {'id': 'teletrabajador_id'}, {'id': 'fecha_hora'}, {'id': 'luz'}, {'id': 'temperatura'}, {'id': 'pausa_activa'}, {'id': 'ruido'}]},
                    ], label_key='id', on_select=lambda e: insertar_tag(e))
                    
            with ui.card().classes('w-1/2 h-72'):
                ui.textarea(label='Escriba la instrucción SQL', placeholder='').bind_value(globals(), 'glob_sql').classes('w-full h-64').props('outlined dense rows=10')
                with ui.row():
                    ui.button('Ejecutar', icon="play_arrow", on_click=lambda: ejecutar_consulta())
                    ui.button('fichero CSV',icon="file_download", on_click=lambda: exportar_csv())

        @ui.refreshable
        def grilla_ui():
            global glob_grilla
            with ui.card().classes('w-full'):
                ui.label('DATOS').classes('font-bold')
                ui.aggrid(glob_grilla).classes('w-full h-64')
        grilla_ui()

        def insertar_tag(e):
            global glob_sql
            glob_sql=glob_sql + " "+e.value

        def ejecutar_consulta():
            global glob_sql
            global glob_grilla
            global conexion_remota
            try:
                if glob_sql.strip()[0:6].upper()=="SELECT":
                    with conexion_remota.cursor() as cursor:
                        cursor.execute(glob_sql)
                        result = cursor.fetchall()
                        glob_grilla['columnDefs']=[]
                        glob_grilla['rowData']=[]
                        n = 0
                        for fila in result:
                            n = n + 1
                            if n == 1:
                                for campo in result[0]:
                                    glob_grilla['columnDefs'].append({
                                        'headerName':campo,
                                        'field':campo
                                    })
                            glob_grilla['rowData'].append(fila)
                        grilla_ui.refresh()
                else:
                    ui.notify('Consulta inválida, solo se permiten consultas de tipo SELECT', close_button='OK',type='negative')
            except Exception as err:
                ui.notify('Error en la consulta:' + str(err), close_button='OK',type='negative')
            
        def exportar_csv():
            global glob_sql
            global glob_grilla
            global conexion_remota
            try:
                with conexion_remota.cursor() as cursor:
                    cursor.execute(glob_sql)
                    result = cursor.fetchall()
                    archivo = f'descargas/{uuid.uuid4()}.csv'
                    pt_archivo = open(archivo, 'w')
                    c = csv.writer(pt_archivo,delimiter=",",quotechar='"',quoting=1,lineterminator="\n")
                    n=0
                    for fila in result: 
                        n= n + 1
                        if n==1:
                            c.writerow(fila.keys())
                        c.writerow(fila.values())
                    pt_archivo.close()
                    ui.download(archivo)
            except:
                ui.notify('Consulta inválida', close_button='OK',type='negative')

    elif pagina=="ver_configuración":
        with ui.card():
            ui.label('DATOS DE LA EMPRESA').classes('font-bold')
            ui.separator()
            with ui.grid(columns=4):
                ui.label("NIT:")
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 12}
                ).bind_value(globals(), 'conf_nit').props('outlined dense').classes('w-64')
                ui.label("Razón Social:").classes('pl-16')
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_nombre').props('outlined dense')
        with ui.card():
            ui.label('CONFIGURACION DE CONEXION AL SERVIDOR DE CORREOS').classes('font-bold')
            ui.separator()
            with ui.grid(columns=4):
                ui.label("Host correo saliente:")
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_host_correo_saliente').props('outlined dense').classes('w-64')
                ui.label("Usuario correo saliente:").classes('pl-16')
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_usuario_correo_saliente').props('outlined dense')
                ui.label("Contraseña:")
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_contraseña_correo_saliente').props('outlined dense type=password')
                ui.label("De:").classes('pl-16')
                ui.input(placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_de_correo_saliente').props('outlined dense')
                ui.label("Para:")
                ui.input( placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 256}
                ).bind_value(globals(), 'conf_para_correo_saliente').props('outlined dense')
                ui.label("Puerto:").classes('pl-16')
                ui.input( placeholder='',
                    validation={'Demasiado largo el campo': lambda value: len(value) < 12}
                ).bind_value(globals(), 'conf_puerto_correo_saliente').props('outlined dense')
        with ui.card():
            ui.label('PARÁMETROS PARA MEDICIONES').classes('font-bold')
            ui.separator()
            with ui.grid(columns=3):
                ui.label("Sensor").classes('font-bold')
                ui.label("Desde").classes('font-bold')
                ui.label("Hasta").classes('font-bold')
                ui.label("Temperatura ideal (C):")
                ui.number(placeholder='').bind_value(globals(), 'conf_temperatura_mínima').props('outlined dense').classes('w-32')
                ui.number(placeholder='').bind_value(globals(), 'conf_temperatura_máxima').props('outlined dense').classes('w-32')  
                ui.label("Luz ambiental ideal (lux):")
                ui.number(placeholder='').bind_value(globals(), 'conf_luz_mínima').props('outlined dense').classes('w-32')
                ui.number(placeholder='').bind_value(globals(), 'conf_luz_máxima').props('outlined dense').classes('w-32')
                ui.label("Ruido ambiental ideal (db):")
                ui.number(placeholder='').bind_value(globals(), 'conf_ruido_mínimo').props('outlined dense').classes('w-32')
                ui.number(placeholder='').bind_value(globals(), 'conf_ruido_máximo').props('outlined dense').classes('w-32')
                ui.label("Detección de presencia (cm):")
                ui.label("0")
                ui.number(placeholder='').bind_value(globals(), 'conf_distancia_ideal').props('outlined dense').classes('w-32')
            ui.separator()
            with ui.grid(columns=4):
                ui.label("Sugerir pausas activas en (horas):")
                ui.number(placeholder='').bind_value(globals(), 'conf_tiempo_pausa_activa').props('outlined dense').classes('w-32')
                ui.label("Espera entre notificaciones seguidas (seg):")
                ui.number(placeholder='').bind_value(globals(), 'conf_espera_entre_notificaciones').props('outlined dense').classes('w-32') 
                ui.label("Frecuencia de envio a repositorio central (seg):")
                ui.number(placeholder='').bind_value(globals(), 'conf_espera_entre_envíos_a_repositorio').props('outlined dense').classes('w-32') 
        with ui.row():
            ui.button('Guardar',icon="save", on_click=guardar_datos)
    elif pagina=="teletrabajadores":
        global glob_borrar
        leer_configuración_remota()
        glob_borrar=False
        registro_teletrabajador_nuevo()
        with ui.column():
            with ui.row():
                grilla_teletrabajadores(obtener_teletrabajadores())
                with ui.column():
                    with ui.card().classes("w-full"):
                        ui.button('Nuevo',icon='add_circle',on_click=nuevo_teletrabajador)
                    with ui.card():
                        with ui.grid(columns=2):
                            ui.label("Cédula:")
                            ui.input(placeholder='').props('outlined dense').bind_value(globals(),'trab_cédula')
                            ui.label("Nombre:")
                            ui.input(placeholder='').bind_value(globals(),'trab_nombre').props('outlined dense').bind_value(globals(),'trab_nombre')
                            ui.label("Género:")
                            ui.select({'M': 'Masculino', 'F': 'Femenino'}).props('outlined dense').bind_value(globals(),'trab_género')
                            ui.label("Fecha de nacimiento:")
                            with ui.input(placeholder='aaaa-mm-dd').props('outlined dense').bind_value(globals(),'trab_fecha_nacimiento') as date:
                                with date.add_slot('append'):
                                    ui.icon('edit_calendar').on('click', lambda: menu.open()).classes('cursor-pointer')
                                with ui.menu() as menu:
                                    ui.date().bind_value(date)
                            ui.label("Fecha de ingreso:")
                            with ui.input(placeholder='aaaa-mm-dd').props('outlined dense').bind_value(globals(),'trab_fecha_ingreso') as date:
                                with date.add_slot('append'):
                                    ui.icon('edit_calendar').on('click', lambda: menu.open()).classes('cursor-pointer')
                                with ui.menu() as menu:
                                    ui.date().bind_value(date)
                            ui.label("Departamento:")
                            ui.select(obtener_departamentos(),on_change=lambda e:cambiar_ciudad(e)).props('outlined dense').bind_value(globals(),'trab_departamento_id')
                            ui.label("Ciudad:")
                            glob_sel_ciudad=ui.select({}).props('outlined dense').bind_value(globals(),'trab_ciudad_id')
                            ui.label("Correo electrónico:")
                            ui.input(placeholder='').props('outlined dense').bind_value(globals(),'trab_correo')
                            ui.space()
                            with ui.row():
                                ui.space()
                                ui.button('Borrar', icon='delete',on_click=borrar_teletrabajador).classes('bg-red').bind_enabled(globals(),'glob_borrar') 
                                ui.button('Guardar', icon='save',on_click=guardar_teletrabajador)
                            
    elif pagina=="salir":
        with ui.card().classes('absolute-center').classes('bg-slate-200'):
            ui.label("¿Desea Salir del sistema?").classes('font-bold')
            ui.separator()
            with ui.row().classes('pt-4'):
                ui.button('Salir', icon='exit_to_app',on_click=salir).classes('bg-red')

def cambiar_ciudad(e):
    global trab_departamento_id
    global glob_sel_ciudad
    if glob_sel_ciudad != None:
        glob_sel_ciudad.set_options(obtener_ciudades(trab_departamento_id))

# procedimiento para hacer logout
def salir(e):
    app.storage.user.clear()
    ui.open('login')
        
# procedimiento que permite el manejo al hacer click en un teletrabajador de la lista que se muestra        
def tabla_select(e):
    global trab_id
    global trab_empresa_id
    global trab_cédula
    global trab_nombre
    global trab_género
    global trab_fecha_nacimiento
    global trab_fecha_ingreso
    global trab_departamento_id
    global trab_ciudad_id
    global trab_correo
    global glob_borrar
    if len(e.selection)==0:
        glob_borrar=False
        registro_teletrabajador_nuevo()
    else:
        glob_borrar=True
        trab_id = e.selection[0]['teletrabajador_id']
        trab_empresa_id = e.selection[0]['empresa_id']
        trab_nombre = e.selection[0]['nombre']
        trab_cédula = e.selection[0]['cédula']
        trab_nombre = e.selection[0]['nombre']
        trab_género = e.selection[0]['género']
        trab_fecha_nacimiento = e.selection[0]['fecha_nacimiento']
        trab_fecha_ingreso = e.selection[0]['fecha_ingreso']
        trab_departamento_id = e.selection[0]['departamento_id']
        glob_sel_ciudad.set_value(e.selection[0]['ciudad_id'])
        trab_ciudad_id = e.selection[0]['ciudad_id']
        trab_correo = e.selection[0]['correo']

# procedimiento para el manejo del evento al hacer click para
# crear una pantalla vacia para el registro de un nuevo teletrabajador
def nuevo_teletrabajador(e):
    registro_teletrabajador_nuevo()

# procedimiento para guardar en la base de datos centralizada los datos
# digitados para un teletrabajador
def guardar_teletrabajador(e):
    global trab_id
    global trab_empresa_id
    global configuración
    global conexion_remota
    global trab_cédula
    global trab_nombre
    global trab_género
    global trab_fecha_nacimiento
    global trab_fecha_ingreso
    global trab_departamento_id
    global trab_ciudad_id
    global trab_correo
    if trab_id !=0:
        try:
            with conexion_remota.cursor() as cursor:
                sql = """ 
                    UPDATE TELETRABAJADOR SET
                        empresa_id = %s,
                        cédula = %s, 
                        nombre = %s,
                        género = %s, 
                        fecha_nacimiento = %s, 
                        fecha_ingreso = %s, 
                        ciudad_id = %s, 
                        correo = %s
                    WHERE
                        teletrabajador_id = %s
                """
                cursor.execute(sql,(trab_empresa_id,trab_cédula,trab_nombre,trab_género,trab_fecha_nacimiento,trab_fecha_ingreso,trab_ciudad_id,trab_correo,trab_id))
                cursor.execute('COMMIT')
                registro_teletrabajador_nuevo()
                ui.notify('Datos guardados', close_button='OK', type='positive')
        except:
            ui.notify('Error de conexión. no se pudieron guardar los datos', close_button='OK',type='negative')
    else:
        try:
            with conexion_remota.cursor() as cursor:
                sql = """ 
                    INSERT INTO TELETRABAJADOR 
                        (empresa_id,cédula,nombre,género,fecha_nacimiento,fecha_ingreso,ciudad_id,correo)
                    VALUES
                        (%s,%s,%s,%s,%s,%s,%s,%s)
                """
                cursor.execute(sql,(trab_empresa_id,trab_cédula,trab_nombre,trab_género,trab_fecha_nacimiento,trab_fecha_ingreso,trab_ciudad_id,trab_correo))
                cursor.execute('COMMIT')
                registro_teletrabajador_nuevo()
                ui.notify('Datos guardados', close_button='OK', type='positive')
        except:
                ui.notify('Error de conexión. no se pudieron guardar los datos', close_button='OK',type='negative')                        

# procedimiento para borrar los datos de un teletrabajador
async def borrar_teletrabajador(e):
    global configuración
    global conexion_remota
    global trab_id
    with ui.dialog() as dialog, ui.card():
        ui.label('¿Está seguro de borrar este registro?')
        with ui.row():
            ui.button('Sí', on_click=lambda: dialog.submit('Yes'),icon='delete').classes('bg-red')
            ui.button('No', on_click=lambda: dialog.submit('No'))
    resultado = await dialog
    if resultado=="Yes":
        try:
            with conexion_remota.cursor() as cursor:
                sql = """ 
                    DELETE FROM TELETRABAJADOR
                    WHERE
                        teletrabajador_id = %s
                """
                cursor.execute(sql,(trab_id))
                cursor.execute('COMMIT')
                registro_teletrabajador_nuevo()
                ui.notify('Registro eliminado con exito', close_button='OK', type='positive')
        except:
                ui.notify('Error de conexión. no se pudieron guardar los datos', close_button='OK',type='negative')

# procedimiento inicial para cargar los datos de la pantalla de gestion
# de teletrabajadores y dejarlo en modo para entrada de un nuevo registro
def registro_teletrabajador_nuevo():
    global trab_id
    global trab_empresa_id
    global configuración
    global conexion_remota
    global trab_cédula
    global trab_nombre
    global trab_género
    global trab_fecha_nacimiento
    global trab_fecha_ingreso
    global trab_departamento_id
    global trab_ciudad_id
    global trab_correo
    trab_id=0
    trab_empresa_id=configuración["empresa"]["id"]
    trab_cédula=""
    trab_nombre=""
    trab_género=""
    trab_fecha_nacimiento=""
    trab_fecha_ingreso=""
    trab_departamento_id=0
    trab_ciudad_id=0
    trab_correo=""
    grilla_teletrabajadores.refresh(obtener_teletrabajadores())

# procedimiento para guardar los cambios realizados 
# a la empresa parametrizada en el archivo de configuración
def guardar_datos():
    global configuración
    global conexion_remota
    global conf_nit
    global conf_nombre
    global conf_usuario
    global conf_contraseña
    global conf_host_correo_saliente
    global conf_usuario_correo_saliente
    global conf_contraseña_correo_saliente
    global conf_de_correo_saliente
    global conf_para_correo_saliente
    global conf_puerto_correo_saliente
    global conf_luz_mínima
    global conf_luz_máxima
    global conf_ruido_mínimo
    global conf_ruido_máximo
    global conf_temperatura_mínima
    global conf_temperatura_máxima
    global conf_distancia_ideal
    global conf_tiempo_pausa_activa
    global conf_espera_entre_notificaciones
    global conf_espera_entre_envíos_a_repositorio
    try:
        with conexion_remota.cursor() as cursor:
            sql = """ 
                UPDATE EMPRESA SET
                    nit = %s, 
                    nombre = %s,
                    host_correo_saliente = %s, 
                    usuario_correo_saliente = %s, 
                    contraseña_correo_saliente = %s, 
                    de_correo_saliente = %s, 
                    para_correo_saliente = %s, 
                    puerto_correo_saliente = %s,
                    luz_mínima = %s,
                    luz_máxima = %s,
                    ruido_mínimo = %s,
                    ruido_máximo = %s,
                    temperatura_mínima = %s,
                    temperatura_máxima = %s,
                    distancia_ideal = %s,
                    tiempo_pausa_activa = %s,
                    espera_entre_notificaciones = %s,
                    espera_entre_envíos_a_repositorio = %s
                WHERE
                    empresa_id = %s
            """
            cursor.execute(sql,(
                conf_nit,
                conf_nombre,
                conf_host_correo_saliente,
                conf_usuario_correo_saliente,
                conf_contraseña_correo_saliente,
                conf_de_correo_saliente,
                conf_para_correo_saliente,
                conf_puerto_correo_saliente,
                conf_luz_mínima,
                conf_luz_máxima,
                conf_ruido_mínimo,
                conf_ruido_máximo,
                conf_temperatura_mínima,
                conf_temperatura_máxima,
                conf_distancia_ideal,
                conf_tiempo_pausa_activa,
                conf_espera_entre_notificaciones,
                conf_espera_entre_envíos_a_repositorio,
                configuración["empresa"]["id"])
            )
            ui.notify('Datos guardados', close_button='OK', type='positive')
    except:
            ui.notify('Error de conexión. no se pudieron guardar los datos', close_button='OK',type='negative')

# procedimiento para leer al archivo de configuración
def leer_configuracion():
    global directorio_actual
    global configuración
    global conexion_remota
    
    directorio_actual = os.getcwd()
    configuración = leer_configuración_local(os.path.join(directorio_actual,NOMBRE_FICHERO_CONFIGURACIÓN))
    conexion_remota = obtener_conexión_remota()
    leer_configuración_remota()

# procedimiento para obtener los registros de la tabla departamentos del repositorio central
def obtener_departamentos():
    global configuración
    global conexion_remota
    try:
        with conexion_remota.cursor() as cursor:
            sql = """ 
                SELECT
                    departamento_id,
                    nombre
                FROM 
                    DEPARTAMENTO
                ORDER BY
                    nombre
            """
            cursor.execute(sql)
            resultado = cursor.fetchall()
            resultado2 = {}
            for fila in resultado:
                resultado2[fila['departamento_id']]=fila['nombre']
            return resultado2
    except:
        ui.notify('Error de conexión. no se pudieron obtener los datos', close_button='OK',type='negative')
        return []

# procedimiento para obtener los registros de la tabla ciudades de un departamento dado
def obtener_ciudades(id_departamento):
    global configuración
    global conexion_remota
    try:
        with conexion_remota.cursor() as cursor:
            sql = """ 
                SELECT
                    ciudad_id,
                    nombre
                FROM 
                    CIUDAD
                WHERE
                    departamento_id = %s
                ORDER BY
                    nombre
            """
            cursor.execute(sql,(id_departamento,))
            resultado = cursor.fetchall()
            resultado2 = {}
            for fila in resultado:
                resultado2[fila['ciudad_id']]=fila['nombre']
            return resultado2
    except:
        ui.notify('Error de conexión. no se pudieron obtener los datos', close_button='OK',type='negative')
        return []

# procedimiento para obtener los registros de la tabla teletrabajadores para la empresa parametrizada
def obtener_teletrabajadores():
    global configuración
    global conexion_remota
    try:
        with conexion_remota.cursor() as cursor:
            sql = """ 
                SELECT
                    teletrabajador_id,
                    empresa_id,
                    cédula, 
                    TELETRABAJADOR.nombre nombre,
                    género,
                    fecha_nacimiento,
                    fecha_ingreso,
                    DEPARTAMENTO.departamento_id departamento_id,
                    TELETRABAJADOR.ciudad_id ciudad_id,
                    correo
                FROM 
                    TELETRABAJADOR,
                    CIUDAD,
                    DEPARTAMENTO
                WHERE
                    empresa_id = %s AND
                    TELETRABAJADOR.ciudad_id = CIUDAD.ciudad_id AND
                    CIUDAD.departamento_id = DEPARTAMENTO.departamento_id
                ORDER BY
                    TELETRABAJADOR.nombre
            """
            cursor.execute(sql, configuración["empresa"]["id"])
            resultado = cursor.fetchall()
            return resultado
    except:
        ui.notify('Error de conexión. no se pudieron obtener los datos', close_button='OK',type='negative')
        return []

app.on_startup(leer_configuracion)

ui.run(storage_secret='ALGO SECRETO XCVBTYDGTYUS998983572')
